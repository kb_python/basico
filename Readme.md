# CURSO BASICO DE PAYTHON



## Ejecucion de Paython
    en linux
    `pytnon3 archivo.py`

## Namespace
    `def`   --> Para crear funciones

    `def imprimir_mensaje():                            --> Despues de los puntos debe existir 4 espacions.
        print('mensaje especial:')
        print('Estoy aprendiendo a usar funciones') 


## Funciones con Cadenas de caracteres
    `upper()`         --> para convertir en mayusculas
    `capitalize()`    --> para convertir la primera letra en mayuscula
    `strip()`         --> para eliminar los espacios en blanco
    `lower()`         --> para covertir en minuscula
    `replace(x,y)`    --> remplaza caracteres por otro
    `len`             --> cantidad de caracteres  
    `caracter[0]`     --> extrae el caracter en la posicion 0, es decir 'c'

    slices            --> dividir una cadena
    `caracter[0:3]`   --> obtiene desde 0 hasta ANTES de la posicion 3, es decir 'car'
    `caracter[:3]`    --> obtiene desde 0 hasta ANTES de la posicion 3, es decir 'car'
    `caracter[3:]`    --> obtiene desde 3 hasta FINAL, es decir 'acter'
    `caracter[1:7:2]` --> obtiene desde 1 hasta ANTES de la posicion 7 en dos en dos, es decir 'aat'
    `caracter[::]`    --> obtiene todo el caracter
    `caracter[1::3]`  --> obtiene desde 1 hasta FINAL de la posicion, en TRES en TRES
    `caracter[::-1]`  --> obtiene desde FINAL hasta el inicial de la posicion. 

    
## Comentario  
    `Ctrl + /`        --> Para realizar comentarios #

## Estructura de datos
    `mi_lista = [a, 5, 5, 'hola']`    --> Lista, son variables
    `mi_lista = (a, 5, 5, 'hola')`    --> Tupla, son estaticas y son eficiente.
    `mi_dicionario = { 'key1': 1, 'key2': 2, 'key3': 3}   --> diccionarios.`



