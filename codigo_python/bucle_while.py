# todas las potencia de 2 ---> 2(2)  2(3) ...  2(1000)

""" contador = 0

print('2 elevado a ' + str(contador) + ' es igual a: ' + str(2**contador))
 """



# Funcion principal
def run():
    LIMITE = 1000000
    contador = 0
    potencia_2 = 2**contador
    while potencia_2 < LIMITE:
        print('2 elevado a ' + str(contador) + ' es igual a: ' + str(potencia_2))
        contador += 1
        potencia_2 = 2**contador



# punto de entrada 
if __name__ == "__main__":
    run()