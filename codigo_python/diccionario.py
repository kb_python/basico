def run():
    # las llaves sriven para definir diccionario
    #Un diccionario: es una estructura de llaves y valores
    mi_diccionario = {
        'llave1':1,
        'llave2':2,
        'llave3':3        
    }

    # print(mi_diccionario['llave1'])
    # print(mi_diccionario['llave2'])
    # print(mi_diccionario['llave3'])

    poblacion_paises = {
        'Argentina':44491585,
        'Brasil':548154528,
        'Colombia':15215151,
        'Peru':5451215
    }

    # print(poblacion_paises['Peru'])


    # Recorrer un dicionario a traves de sus llaves
    # for pais in poblacion_paises.keys():
    #      print(pais)


    # Recorrer un dicionario a traves de sus valores
    # for numero_poblacion in poblacion_paises.values():
    #     print(numero_poblacion)
    

    # Recorrer un dicionario y mostrar el ITEM (clave y valor)
    for pais, poblacion in poblacion_paises.items():
        print('El Pais: ' + pais + ' tiene> ' + str(poblacion) + ' de poblacion' )

 
if __name__ == "__main__":
    run()