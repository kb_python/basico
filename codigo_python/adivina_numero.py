import random

def run():
    numero_aleatorio = random.randint(0,100)

    numero_elegido = int(input('Eligen un numero de 1 al 100'))

    while numero_elegido != numero_aleatorio:
        if(numero_elegido < numero_aleatorio):
            print('Busca un numero mas grande')
        else:
            print('Busca un numero mas pequeno')
        numero_elegido = int(input('Eligen otro numero'))

        
    print('GANASTES..!!')


if __name__ == "__main__":
    run()