import random

def generate_password():
    mayusculas = ['A','B','C','D','E','F','G']
    minusculas = ['a','b','c','d','e','f','g']
    simbolos = ['!','#','$','%','/','*','(']
    numeros = ['1','2','3','4','5','6','7']

    caracteres = mayusculas + minusculas + simbolos + numeros

    passowrd = []

    for i in range(15):
        caracter_random = random.choice(caracteres)
        passowrd.append(caracter_random)

    #convertir un LISTA a un STRING
    passowrd = ''.join(passowrd)
    return passowrd


def run():
    passowrd = generate_password()
    print('Tu nueva contrase;a es:  ' + passowrd)

if __name__ == "__main__":
    run()